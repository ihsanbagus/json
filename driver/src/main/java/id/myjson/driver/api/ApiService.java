package id.myjson.driver.api;

import java.util.List;

import id.myjson.driver.model.AuthModel;
import id.myjson.driver.model.DaerahModel;
import id.myjson.driver.model.ProfileModel;
import id.myjson.driver.model.ReturnModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("Auth/signin")
    Call<ReturnModel<List<AuthModel>>> getSignin(@Header("X-Api-Key") String auth, @Query("param") String param);

    @FormUrlEncoded
    @POST("Auth/signup")
    Call<ReturnModel> postSignup(@Header("X-Api-Key") String auth, @Field("param") String param);

    @GET("Users")
    Call<ReturnModel<List<ProfileModel>>> getUserBy(@Header("X-Api-Key") String auth, @Query("no_hp") String user_id);

    @FormUrlEncoded
    @POST("Users")
    Call<ReturnModel> addUser(@Header("X-Api-Key") String auth, @Field("param") String param);

    @GET("Wilayah/{uri}/{daerah_id}")
    Call<ReturnModel<List<DaerahModel>>> getDaerah(@Header("X-Api-Key") String auth, @Path("uri") String uri, @Path("daerah_id") String daerah_id);

    // previous code for single file uploads
    @Multipart
    @POST("UploadFile")
    Call<ReturnModel> uploadFile(
            @Part("description") RequestBody description,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST("Auth/register")
    Call<ReturnModel> signUp(
            @Header("X-Api-Key") String auth,
            @Part("nama") RequestBody nama,
            @Part("tempat_lahir") RequestBody tempat_lahir,
            @Part("tanggal_lahir") RequestBody tanggal_lahir,
            @Part("jenis_kelamin") RequestBody jenis_kelamin,
            @Part("alamat") RequestBody alamat,
            @Part("rt") RequestBody rt,
            @Part("rw") RequestBody rw,
            @Part("no_rumah") RequestBody no_rumah,
            @Part("provinsi") RequestBody provinsi,
            @Part("kota") RequestBody kota,
            @Part("kecamatan") RequestBody kecamatan,
            @Part("kelurahan") RequestBody kelurahan,
            @Part("kewarganegaraan") RequestBody kewarganegaraan,
            @Part("no_kk") RequestBody no_kk,
            @Part("no_ktp") RequestBody no_ktp,
            @Part MultipartBody.Part foto_ktp,
            @Part MultipartBody.Part foto_domisili,
            @Part MultipartBody.Part foto_profil
    );

    @Multipart
    @POST("Auth/register")
    Call<ReturnModel> signUpx(
            @Header("X-Api-Key") String auth,
            @Part("no_ktp") RequestBody no_ktp,
            @Part MultipartBody.Part foto_ktp,
            @Part MultipartBody.Part foto_domisili,
            @Part MultipartBody.Part foto_profil
    );
}
