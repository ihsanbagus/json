package id.myjson.driver.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static String BASE_URL = "http://bagus.explorindo.co.id/ApiUserLocation/api/";
    private static String BASE_URL_LOCAL = "http://10.0.1.200/ApiUserLocation/api/";
    public static String Authorization = "IhsanBagus";

    public static Retrofit getClient() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
