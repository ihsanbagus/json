package id.myjson.driver.model;

import com.google.gson.annotations.SerializedName;

public class ReturnModel<O> {

    @SerializedName("pesan")
    private String pesan;

    @SerializedName("statusKode")
    private int statusKode;

    @SerializedName("data")
    private O data;

    @SerializedName("status")
    private boolean status;

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getPesan() {
        return pesan;
    }

    public void setStatusKode(int statusKode) {
        this.statusKode = statusKode;
    }

    public int getStatusKode() {
        return statusKode;
    }

    public void setData(O data) {
        this.data = data;
    }

    public O getData() {
        return data;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "ReturnModel{" +
                        "pesan = '" + pesan + '\'' +
                        ",statusKode = '" + statusKode + '\'' +
                        ",data = '" + data + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}