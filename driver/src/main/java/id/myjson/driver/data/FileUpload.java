package id.myjson.driver.data;

import android.util.Log;

import androidx.annotation.NonNull;

import id.myjson.driver.interfaces.IFileUpload;
import id.myjson.driver.model.ReturnModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileUpload extends ApiPresenter {
    private IFileUpload iFileUpload;

    public FileUpload(IFileUpload iFileUpload) {
        this.iFileUpload = iFileUpload;
    }

    public void doUpload(RequestBody filename, MultipartBody.Part fileToUpload) {
        Call<ReturnModel> call = apiService.uploadFile(filename, fileToUpload);
        call.enqueue(new Callback<ReturnModel>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel> call, @NonNull Response<ReturnModel> response) {
                if (response.body().isStatus()) {
                    iFileUpload.onUploadSuccess(response.body());
                } else {
                    iFileUpload.onUploadFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel> call, @NonNull Throwable t) {
                iFileUpload.onUploadFailure("Tidak dapat terhubung ke Server, periksa koneksi internet atau hubungi Admin.");
                Log.e("Failure", t.getMessage());
                call.cancel();
            }
        });
    }
}
