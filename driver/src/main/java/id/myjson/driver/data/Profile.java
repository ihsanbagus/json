package id.myjson.driver.data;

import androidx.annotation.NonNull;

import java.util.List;

import id.myjson.driver.api.ApiClient;
import id.myjson.driver.interfaces.IProfile;
import id.myjson.driver.model.ProfileModel;
import id.myjson.driver.model.ReturnModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profile extends ApiPresenter {
    private IProfile iProfile;

    public Profile(IProfile iProfile) {
        this.iProfile = iProfile;
    }

    public void getProfile(String param) {
        Call<ReturnModel<List<ProfileModel>>> call = apiService.getUserBy(ApiClient.Authorization, param);
        call.enqueue(new Callback<ReturnModel<List<ProfileModel>>>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel<List<ProfileModel>>> call, @NonNull Response<ReturnModel<List<ProfileModel>>> response) {
                if (response.body().isStatus()) {
                    List<ProfileModel> data = response.body().getData();
                    iProfile.onLoadDataSuccess(data);
                } else {
                    iProfile.onLoadDataFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel<List<ProfileModel>>> call, @NonNull Throwable t) {
                iProfile.onLoadDataFailure(t.getMessage());
                call.cancel();
            }
        });
    }
}
