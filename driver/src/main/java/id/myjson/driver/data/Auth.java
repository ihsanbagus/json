package id.myjson.driver.data;

import androidx.annotation.NonNull;

import java.util.List;

import id.myjson.driver.api.ApiClient;
import id.myjson.driver.interfaces.IAuth;
import id.myjson.driver.model.AuthModel;
import id.myjson.driver.model.ReturnModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Auth extends ApiPresenter {
    private IAuth.ISignin iSignin;
    private IAuth.ISignup iSignup;

    public Auth(IAuth.ISignin iSignin, IAuth.ISignup iSignup) {
        this.iSignin = iSignin;
        this.iSignup = iSignup;
    }

    public void getSignin(String param) {
        Call<ReturnModel<List<AuthModel>>> call = apiService.getSignin(ApiClient.Authorization, param);
        call.enqueue(new Callback<ReturnModel<List<AuthModel>>>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel<List<AuthModel>>> call, @NonNull Response<ReturnModel<List<AuthModel>>> response) {
                if (response.body().isStatus()) {
                    List<AuthModel> data = response.body().getData();
                    iSignin.onSigninSuccess(data);
                } else {
                    iSignin.onSigninFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel<List<AuthModel>>> call, @NonNull Throwable t) {
                iSignin.onSigninFailure(t.getMessage());
                call.cancel();
            }
        });
    }

    public void postSignup(String param) {
        Call<ReturnModel> call = apiService.postSignup(ApiClient.Authorization, param);
        call.enqueue(new Callback<ReturnModel>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel> call, @NonNull Response<ReturnModel> response) {
                if (response.body().isStatus()) {
                    ReturnModel data = response.body();
                    iSignup.onSignupSuccess(data.getPesan());
                } else {
                    iSignup.onSignupFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel> call, @NonNull Throwable t) {
                iSignin.onSigninFailure(t.getMessage());
                call.cancel();
            }
        });
    }

    public void signUp(RequestBody nama, RequestBody tempat_lahir, RequestBody tanggal_lahir, RequestBody jenis_kelamin, RequestBody alamat, RequestBody rt, RequestBody rw, RequestBody no_rumah, RequestBody provinsi, RequestBody kota, RequestBody kecamatan, RequestBody kelurahan, RequestBody kewarganegaraan, RequestBody no_kk, RequestBody no_ktp, MultipartBody.Part foto_ktp, MultipartBody.Part foto_domisili, MultipartBody.Part foto_profile) {
        Call<ReturnModel> call = apiService.signUp(
                ApiClient.Authorization,
                nama,
                tempat_lahir,
                tanggal_lahir,
                jenis_kelamin,
                alamat,
                rt,
                rw,
                no_rumah,
                provinsi,
                kota,
                kecamatan,
                kelurahan,
                kewarganegaraan,
                no_kk,
                no_ktp,
                foto_ktp,
                foto_domisili,
                foto_profile
        );
        call.enqueue(new Callback<ReturnModel>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel> call, @NonNull Response<ReturnModel> response) {
                if (response.body().isStatus()) {
                    ReturnModel data = response.body();
                    iSignup.onSignupSuccess(data.getPesan());
                } else {
                    iSignup.onSignupFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel> call, @NonNull Throwable t) {
                iSignin.onSigninFailure(t.getMessage());
                call.cancel();
            }
        });
    }

    public void signUpx(RequestBody no_ktp, MultipartBody.Part foto_ktp, MultipartBody.Part foto_domisili, MultipartBody.Part foto_profil) {
        Call<ReturnModel> call = apiService.signUpx(
                ApiClient.Authorization,
                no_ktp,
                foto_ktp,
                foto_domisili,
                foto_profil
        );
        call.enqueue(new Callback<ReturnModel>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel> call, @NonNull Response<ReturnModel> response) {
                if (response.body().isStatus()) {
                    ReturnModel data = response.body();
                    iSignup.onSignupSuccess(data.getPesan());
                } else {
                    iSignup.onSignupFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel> call, @NonNull Throwable t) {
                iSignin.onSigninFailure(t.getMessage());
                call.cancel();
            }
        });
    }

}
