package id.myjson.driver.data;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.List;

import id.myjson.driver.interfaces.IDaerah;
import id.myjson.driver.model.DaerahModel;
import id.myjson.driver.model.ReturnModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.myjson.driver.api.ApiClient.Authorization;

public class Daerah extends ApiPresenter {
    private IDaerah iDaerah;

    public Daerah(IDaerah iDaerah) {
        this.iDaerah = iDaerah;
    }

    public void getDaerah(String daerah_id, String from) {
        Call<ReturnModel<List<DaerahModel>>> call = apiService.getDaerah(Authorization, from, daerah_id);
        call.enqueue(new Callback<ReturnModel<List<DaerahModel>>>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel<List<DaerahModel>>> call, @NonNull Response<ReturnModel<List<DaerahModel>>> response) {
                if (response.body().isStatus()) {
                    List<DaerahModel> data = response.body().getData();
                    iDaerah.onLoadDaerahSuccess(data, from);
                } else {
                    iDaerah.onLoadDaerahFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel<List<DaerahModel>>> call, @NonNull Throwable t) {
                iDaerah.onLoadDaerahFailure("Tidak dapat terhubung ke Server, periksa koneksi internet atau hubungi Admin.");
                Log.e("Failure", t.getMessage());
                call.cancel();
            }
        });
    }
}
