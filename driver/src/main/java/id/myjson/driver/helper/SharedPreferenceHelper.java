package id.myjson.driver.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceHelper {

    public static void createSP(Activity act, String key, String value) {
        SharedPreferences mSettings = act.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String readSP(Activity act, String key) {
        SharedPreferences mSettings = act.getSharedPreferences("settings", Context.MODE_PRIVATE);
        return mSettings.getString(key, "");
    }

    public static void deleteSP(Activity act, String key) {
        SharedPreferences mSettings = act.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.remove(key);
        editor.apply();
    }

}
