package id.myjson.driver.helper;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.widget.AppCompatTextView;

import id.myjson.driver.R;
import id.myjson.driver.interfaces.IDialog;

public class DialogHelper {

    private IDialog iDialog;

    public DialogHelper(IDialog iDialog) {
        this.iDialog = iDialog;
    }

    public void Comfirm(Context ctx, String title, String pesan) {
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(true);

        AppCompatTextView tTitle = dialog.findViewById(R.id.tTitle);
        AppCompatTextView tPesan = dialog.findViewById(R.id.tPesan);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        tTitle.setText(title);
        tPesan.setText(pesan);

        dialog.findViewById(R.id.bClose).setOnClickListener(v -> dialog.dismiss());

        dialog.findViewById(R.id.bTidak).setOnClickListener(v -> {
            dialog.dismiss();
            iDialog.onDialogConfirmClickNo(dialog);
        });

        dialog.findViewById(R.id.bYa).setOnClickListener(v -> {
            dialog.dismiss();
            iDialog.onDialogConfirmClickYes(dialog);
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
