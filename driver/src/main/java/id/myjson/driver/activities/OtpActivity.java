package id.myjson.driver.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.myjson.driver.R;

public class OtpActivity extends BaseActivity {

    @BindView(R.id.kodeOtp)
    AppCompatEditText kodeOtp;
    @BindView(R.id.bKonfirmasi)
    AppCompatButton bKonfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bKonfirmasi})
    public void onViewClicked() {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
