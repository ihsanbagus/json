package id.myjson.driver.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.myjson.driver.R;
import id.myjson.driver.data.Auth;
import id.myjson.driver.data.Daerah;
import id.myjson.driver.data.FileUpload;
import id.myjson.driver.helper.DialogHelper;
import id.myjson.driver.interfaces.IDaerah;
import id.myjson.driver.interfaces.IDialog;
import id.myjson.driver.interfaces.IFileUpload;
import id.myjson.driver.interfaces.IAuth;
import id.myjson.driver.model.DaerahModel;
import id.myjson.driver.model.ReturnModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static id.myjson.driver.helper.AlertHelper.toast;
import static id.myjson.driver.helper.MyHelper.getFileName;
import static id.myjson.driver.helper.MyHelper.randomString;
import static id.myjson.driver.helper.MyHelper.rippleClick;

public class SignupActivity extends BaseActivity implements IDaerah, IDialog, IFileUpload, IAuth.ISignup {

    @BindView(R.id.tgLahir)
    AppCompatTextView tgLahir;
    @BindView(R.id.jenisKelamin)
    AppCompatTextView jenisKelamin;
    @BindView(R.id.provinsi)
    AppCompatTextView provinsi;
    @BindView(R.id.kota)
    AppCompatTextView kota;
    @BindView(R.id.kecamatan)
    AppCompatTextView kecamatan;
    @BindView(R.id.kelurahan)
    AppCompatTextView kelurahan;
    @BindView(R.id.bSignup)
    AppCompatButton bSignup;
    @BindView(R.id.nama)
    AppCompatEditText nama;
    @BindView(R.id.tempatLahir)
    AppCompatEditText tempatLahir;
    @BindView(R.id.alamat)
    AppCompatEditText alamat;
    @BindView(R.id.rt)
    AppCompatEditText rt;
    @BindView(R.id.rw)
    AppCompatEditText rw;
    @BindView(R.id.noRumah)
    AppCompatEditText noRumah;
    @BindView(R.id.wni)
    AppCompatRadioButton wni;
    @BindView(R.id.wna)
    AppCompatRadioButton wna;
    @BindView(R.id.kewarganegaraan)
    RadioGroup kewarganegaraan;
    @BindView(R.id.noKk)
    AppCompatEditText noKk;
    @BindView(R.id.noKtp)
    AppCompatEditText noKtp;
    @BindView(R.id.ktp)
    AppCompatImageView ktp;
    @BindView(R.id.profile)
    AppCompatImageView profile;
    @BindView(R.id.domisili)
    AppCompatImageView domisili;
    private Calendar calendar = Calendar.getInstance();
    private int year, month, day;
    private ProgressDialog loading;
    private Daerah prov = new Daerah(this);
    private FileUpload fUpload = new FileUpload(this);
    private Auth fAuth = new Auth(null, this);
    private String provinsiId = "0", kotaId = "0", kecamatanId = "0";
    private AppCompatTextView tv;
    private String kwn = "";
    private String fKtp = "";
    private String fDomisili = "";
    private String fProfile = "";
    private File uKtp = null;
    private File uDomisili = null;
    private File uProfile = null;
    private static final int TAKE_KTP = 103;
    private static final int TAKE_DOMISILI = 104;
    private static final int TAKE_PROFILE = 105;
    private DialogHelper dh = new DialogHelper(this);
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
//        showDate(year, month + 1, day);
    }

    private void permissionRequest(int permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, permission);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Here, this is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                toast(this, "Aplikasi Membutuhkan Izin");
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, requestCode);
            }
        } else {
            // Permission has already been granted
            //from gallery
            Intent takeProfile = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(takeProfile, requestCode);
        }

        // other 'case' lines to check for other
        // permissions this app might request
    }

    @OnClick({R.id.tgLahir, R.id.jenisKelamin, R.id.provinsi, R.id.kota, R.id.kecamatan, R.id.kelurahan, R.id.wni, R.id.wna, R.id.ktp, R.id.domisili, R.id.profile, R.id.bSignup})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.tgLahir:
                tgLahir.setError(null);
                setDate(v);
                break;
            case R.id.jenisKelamin:
                jenisKelamin.setError(null);
                dialogJenisKelamin();
                break;
            case R.id.provinsi:
                tv = (AppCompatTextView) v;
                prov.getDaerah("", "provinsi");
                loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
                break;
            case R.id.kota:
                tv = (AppCompatTextView) v;
                if (Integer.parseInt(provinsiId) > 0) {
                    prov.getDaerah(provinsiId, "kota");
                    loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
                } else {
                    toast(this, "Pilih Provinsi Dahulu");
                }
                break;
            case R.id.kecamatan:
                tv = (AppCompatTextView) v;
                if (Integer.parseInt(kotaId) > 0) {
                    prov.getDaerah(kotaId, "kecamatan");
                    loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
                } else {
                    toast(this, "Pilih Kota / Kabupaten Dahulu");
                }
                break;
            case R.id.kelurahan:
                tv = (AppCompatTextView) v;
                if (Integer.parseInt(kecamatanId) > 0) {
                    prov.getDaerah(kecamatanId, "kelurahan");
                    loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
                } else {
                    toast(this, "Pilih Kecamatan Dahulu");
                }
                break;
            case R.id.wni:
                wni.setError(null);
                wna.setError(null);
                kwn = "wni";
                break;
            case R.id.wna:
                wni.setError(null);
                wna.setError(null);
                kwn = "wna";
                break;
            case R.id.ktp:
//                Intent takeKtp = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takeKtp, TAKE_KTP);

                //from gallery
//                Intent takeKtp = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(takeKtp, TAKE_KTP);
                permissionRequest(TAKE_KTP);
                break;
            case R.id.domisili:
//                Intent takeKtp = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takeKtp, TAKE_KTP);

                //from gallery
//                Intent takeDomisili = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(takeDomisili, TAKE_DOMISILI);
                permissionRequest(TAKE_DOMISILI);
                break;
            case R.id.profile:
                //from camera
//                Intent takeProfile = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takeProfile, TAKE_PROFILE);

                //from gallery
//                Intent takeProfile = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(takeProfile, TAKE_PROFILE);
                permissionRequest(TAKE_PROFILE);
                break;
            case R.id.bSignup:
//                dh.Comfirm(this, "Konfirmasi Data", "Data Yang Diisi Sudah Benar ?");
                if (cekField()) {
                    dh.Comfirm(this, "Konfirmasi Data", "Data Yang Diisi Sudah Benar ?");
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case TAKE_KTP:
                if (resultCode == RESULT_OK) {
                    Uri uri = imageReturnedIntent.getData();
                    uKtp = createTempFile(uri);
                    fKtp = getFileName(uri);
                    Glide.with(this).load(uri).into(ktp);
                }
                break;
            case TAKE_DOMISILI:
                if (resultCode == RESULT_OK) {
                    Uri uri = imageReturnedIntent.getData();
                    uDomisili = createTempFile(uri);
                    fDomisili = getFileName(uri);
                    Glide.with(this).load(uri).into(domisili);
                }
                break;
            case TAKE_PROFILE:
                if (resultCode == RESULT_OK) {
                    Uri uri = imageReturnedIntent.getData();
                    uProfile = createTempFile(uri);
                    fProfile = getFileName(uri);
                    Glide.with(this).load(uri).into(profile);
                }
                break;
        }
    }

    private boolean cekField() {
        boolean ok = true;
        String pesan = "Wajib Diisi";
        if (TextUtils.isEmpty(nama.getText())) {
            nama.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(tempatLahir.getText())) {
            tempatLahir.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(tgLahir.getText())) {
            tgLahir.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(jenisKelamin.getText())) {
            jenisKelamin.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(alamat.getText())) {
            alamat.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(rt.getText())) {
            rt.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(rw.getText())) {
            rw.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(noRumah.getText())) {
            noRumah.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(provinsi.getText())) {
            provinsi.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(kota.getText())) {
            kota.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(kecamatan.getText())) {
            kecamatan.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(kelurahan.getText()) && !provinsiId.equalsIgnoreCase("91") && !provinsiId.equalsIgnoreCase("94")) {
            kelurahan.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(kwn)) {
            wni.setError(pesan);
            wna.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(noKk.getText())) {
            noKk.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(noKtp.getText())) {
            noKtp.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(fKtp)) {
            toast(this, "Upload Foto KTP");
            ok = false;
        }
        if (TextUtils.isEmpty(fDomisili)) {
            toast(this, "Upload Foto Domisili");
            ok = false;
        }
        if (TextUtils.isEmpty(fProfile)) {
            toast(this, "Upload Foto Profil");
            ok = false;
        }
        return ok;
    }

    private void showDate(int year, int month, int day) {
        tgLahir.setText(new StringBuilder().append(day).append("/").append(month).append("/").append(year));
    }

    public void setDate(View view) {
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = (arg0, arg1, arg2, arg3) -> {
        tgLahir.setText(arg1 + "-" + arg2 + "-" + arg3);// arg1 = year// arg2 = month// arg3 = day
    };

    private void dialogJenisKelamin() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_jenis_kelamin);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        RadioGroup jk = dialog.findViewById(R.id.jenisKelamin);

        jk.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton checkedRadioButton = group.findViewById(checkedId);
            // This puts the value (true/false) into the variable
            boolean isChecked = checkedRadioButton.isChecked();
            // If the radiobutton that has changed in check state is now checked...
            if (isChecked) {
                // Changes the textview's text to "Checked: example radiobutton text"
                jenisKelamin.setText(checkedRadioButton.getText());
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onLoadDaerahSuccess(List<DaerahModel> data, String from) {
        tv.setError(null);
        if (data.size() > 0) {
            showDaerah("Pilih " + from, data, tv, from);
        } else {
            toast(this, "data kosong");
        }
        loading.dismiss();
    }

    @Override
    public void onLoadDaerahFailure(String pesan) {
        toast(this, pesan);
        loading.dismiss();
    }

    @Override
    public void onUploadSuccess(ReturnModel returnModels) {
        toast(this, returnModels.getPesan());
    }

    @Override
    public void onUploadFailure(String pesan) {
        toast(this, pesan);
    }

    private void showDaerah(String judul, List<DaerahModel> data, AppCompatTextView z, String from) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_daerah);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        AppCompatTextView title = dialog.findViewById(R.id.title);
        AppCompatImageButton bClose = dialog.findViewById(R.id.bClose);
        LinearLayoutCompat namaDaerah = dialog.findViewById(R.id.namaDaerah);
        title.setText(judul);
        bClose.setOnClickListener(v -> dialog.dismiss());

        for (DaerahModel x : data) {
            AppCompatTextView a = new AppCompatTextView(this);
            rippleClick(this, a);
            a.setPadding(10, 10, 10, 10);
            a.setTextSize(getResources().getDimension(R.dimen.text_xmedium));
            a.setHint(x.getId());
            a.setText(x.getName());
            a.setOnClickListener(v -> {
                if (from.equalsIgnoreCase("provinsi")) {
                    provinsiId = a.getHint().toString();
                    kotaId = "0";
                    kecamatanId = "0";
                    kota.setText("");
                    kecamatan.setText("");
                    kelurahan.setText("");
                }
                if (from.equalsIgnoreCase("kota")) {
                    kotaId = a.getHint().toString();
                    kecamatanId = "0";
                    kecamatan.setText("");
                    kelurahan.setText("");
                }
                if (from.equalsIgnoreCase("kecamatan")) {
                    kecamatanId = a.getHint().toString();
                    kelurahan.setText("");
                }
                z.setText(a.getText());
                dialog.dismiss();
            });
            namaDaerah.addView(a);
            View v = new View(this);
            v.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            v.setBackgroundColor(getResources().getColor(R.color.black));
            namaDaerah.addView(v);
        }

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onDialogConfirmClickYes(Dialog dialog) {
        loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
        doSignup();
    }

    @Override
    public void onDialogConfirmClickNo(Dialog dialog) {
    }

    private void doSignup() {
        RequestBody xnama = RequestBody.create(MultipartBody.FORM, nama.getText().toString());
        RequestBody xtempat_lahir = RequestBody.create(MultipartBody.FORM, tempatLahir.getText().toString());
        RequestBody xtanggal_lahir = RequestBody.create(MultipartBody.FORM, tgLahir.getText().toString());
        RequestBody xjenis_kelamin = RequestBody.create(MultipartBody.FORM, jenisKelamin.getText().toString());
        RequestBody xalamat = RequestBody.create(MultipartBody.FORM, alamat.getText().toString());
        RequestBody xrt = RequestBody.create(MultipartBody.FORM, rt.getText().toString());
        RequestBody xrw = RequestBody.create(MultipartBody.FORM, rw.getText().toString());
        RequestBody xno_rumah = RequestBody.create(MultipartBody.FORM, noRumah.getText().toString());
        RequestBody xprovinsi = RequestBody.create(MultipartBody.FORM, provinsi.getText().toString());
        RequestBody xkota = RequestBody.create(MultipartBody.FORM, kota.getText().toString());
        RequestBody xkecamatan = RequestBody.create(MultipartBody.FORM, kecamatan.getText().toString());
        RequestBody xkelurahan = RequestBody.create(MultipartBody.FORM, kelurahan.getText().toString());
        RequestBody xkewarganegaraan = RequestBody.create(MultipartBody.FORM, kwn.toString());
        RequestBody xno_kk = RequestBody.create(MultipartBody.FORM, noKk.getText().toString());
        RequestBody xno_ktp = RequestBody.create(MultipartBody.FORM, noKtp.getText().toString());

        RequestBody ktps = RequestBody.create(MediaType.parse("image/*"), uKtp);
        RequestBody domisilis = RequestBody.create(MediaType.parse("image/*"), uDomisili);
        RequestBody profils = RequestBody.create(MediaType.parse("image/*"), uProfile);

        MultipartBody.Part xktp = MultipartBody.Part.createFormData("foto_ktp", uKtp.getName(), ktps);
        MultipartBody.Part xdomisili = MultipartBody.Part.createFormData("foto_dimisili", uDomisili.getName(), domisilis);
        MultipartBody.Part xprofil = MultipartBody.Part.createFormData("foto_profil", uProfile.getName(), profils);

        fAuth.signUp(
                xnama,
                xtempat_lahir,
                xtanggal_lahir,
                xjenis_kelamin,
                xalamat,
                xrt,
                xrw,
                xno_rumah,
                xprovinsi,
                xkota,
                xkecamatan,
                xkelurahan,
                xkewarganegaraan,
                xno_kk,
                xno_ktp,
                xktp,
                xdomisili,
                xprofil
        );

//        fAuth.signUpx(
//                xno_ktp,
//                xktp,
//                xdomisili,
//                xprofil
//        );
    }

    private void uploadFile(Uri uri) {
        File file = createTempFile(uri);
        // Parsing any Media type file
        RequestBody description = RequestBody.create(MultipartBody.FORM, file.getName());
        RequestBody files = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)), file);
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("userFile", file.getName(), files);

        fUpload.doUpload(description, body);
    }

    private File createTempFile(Uri uri) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        @SuppressLint("SimpleDateFormat")
        String name = randomString() + ".jpg";
        File file = new File(getCacheDir(), name);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file

        try {
            FileOutputStream fos = new FileOutputStream(file, true);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public void onSignupSuccess(String pesan) {
        toast(this, pesan);
        loading.dismiss();
        Log.d("berhasil", pesan);
    }

    @Override
    public void onSignupFailure(String pesan) {
        toast(this, pesan);
        loading.dismiss();
        Log.d("gagal", pesan);
    }
}
