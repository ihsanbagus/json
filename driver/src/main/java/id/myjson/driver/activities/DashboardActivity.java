package id.myjson.driver.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.PopupMenu;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.myjson.driver.R;
import id.myjson.driver.helper.DialogHelper;
import id.myjson.driver.interfaces.IDialog;

public class DashboardActivity extends BaseActivity implements IDialog {

    private DialogHelper dh = new DialogHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bSignout})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSignout:
                dh.Comfirm(this, "Sign Out", "Ingin Signout dari aplikasi ?");
                break;
        }
    }

    public void jemput(View v) {
        startActivity(new Intent(this, JemputActivity.class));
    }

    private void options(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_profile:
//                    startActivity(new Intent(this, ProfileActivity.class));
                    break;
                case R.id.menu_setting:
//                    startActivity(new Intent(this, SettingActivity.class));
                    break;
                case R.id.menu_about:
//                    startActivity(new Intent(this, AboutActivity.class));
                    break;
            }
            return true;
        });
        popupMenu.inflate(R.menu.options);
        popupMenu.show();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onDialogConfirmClickYes(Dialog dialog) {

    }

    @Override
    public void onDialogConfirmClickNo(Dialog dialog) {

    }
}