package id.myjson.driver.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.myjson.driver.R;
import id.myjson.driver.helper.DialogHelper;
import id.myjson.driver.interfaces.IDialog;

import static id.myjson.driver.helper.GpsHelper.midPoint;

public class JemputActivity extends BaseActivity implements OnMapReadyCallback, IDialog {

    private GoogleMap mMap;
    private DialogHelper dh = new DialogHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jemput);
        ButterKnife.bind(this);
        initComponent();
    }

    private void initComponent() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        new Handler().postDelayed(() -> {
            if (mMap != null) {
                LatLng ll1 = new LatLng(-6.2484267, 106.841408);
                LatLng ll2 = new LatLng(-6.2430799, 106.8413919);
                LatLng ll3 = midPoint(-6.2484267, 106.841408, -6.2430799, 106.8413919);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ll3, 15.36f));

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(ll1);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title("Rumah");

                //add icon
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_rumah));

                // Placing a marker on the touched position
                mMap.addMarker(markerOptions).showInfoWindow();

                //marker2
                // Creating a marker
                markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(ll2);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title("Petugas");

                //add icon
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_motor));

                // Placing a marker on the touched position
                mMap.addMarker(markerOptions).showInfoWindow();
            }
        }, 300);
    }

    @OnClick({R.id.bSignout, R.id.bAngkut})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.bSignout:
                dh.Comfirm(this, "Sign Out", "Ingin Signout dari aplikasi ?");
                break;
            case R.id.bAngkut:
                if (v.isActivated()) {
//                    v.setEnabled(false);
                    v.setActivated(false);
                } else {
                    startActivity(new Intent(this, LokasiActivity.class));
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latlang = new LatLng(-2.548926, 118.0148634);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlang, 3.36f));
//        mMap.setLatLngBoundsForCameraTarget(new LatLngBounds(new LatLng(-2.548926, 118.0148634), new LatLng(-2.548926, 118.0148634)));
        mMap.setMinZoomPreference(3.36f);
        mMap.setMyLocationEnabled(false);
        mMap.setOnMyLocationButtonClickListener(() -> {
            new Handler().postDelayed(() -> {
                if (mMap != null) {
//                    LatLng latlang = new LatLng(latitude, longitude);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlang, 3.36f));
                }
            }, 100);
            return false;
        });
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onDialogConfirmClickYes(Dialog dialog) {

    }

    @Override
    public void onDialogConfirmClickNo(Dialog dialog) {

    }
}
