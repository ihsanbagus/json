package id.myjson.driver.interfaces;

import java.util.List;

import id.myjson.driver.model.ProfileModel;

public interface IProfile {
    void onLoadDataSuccess(List<ProfileModel> data);
    void onLoadDataFailure(String pesan);
}
