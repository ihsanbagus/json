package id.myjson.driver.interfaces;

import java.util.List;

import id.myjson.driver.model.DaerahModel;

public interface IDaerah {
    void onLoadDaerahSuccess(List<DaerahModel> data, String from);

    void onLoadDaerahFailure(String pesan);
}
