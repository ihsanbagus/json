package id.myjson.driver.interfaces;

import java.util.List;

import id.myjson.driver.model.AuthModel;

public interface IAuth {

    interface ISignin {
        void onSigninSuccess(List<AuthModel> data);
        void onSigninFailure(String pesan);
    }

    interface ISignup {
        void onSignupSuccess(String pesan);
        void onSignupFailure(String pesan);
    }

}
