package id.myjson.driver.interfaces;

import id.myjson.driver.model.ReturnModel;

public interface IFileUpload {
    void onUploadSuccess(ReturnModel returnModels);

    void onUploadFailure(String pesan);
}
