package id.myjson.driver.interfaces;

public interface ISignup {
    void onLoadDataSuccess(String pesan);
    void onLoadDataFailure(String pesan);
}
