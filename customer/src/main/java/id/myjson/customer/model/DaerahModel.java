package id.myjson.customer.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DaerahModel {

    @SerializedName("statusKode")
    private int statusKode;

    @SerializedName("status")
    private boolean status;

    @SerializedName("pesan")
    private String message;

    @SerializedName("data")
    private List<DaerahItem> data;

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean status() {
        return status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getStatusKode() {
        return statusKode;
    }

    public void setStatusKode(int statusKode) {
        this.statusKode = statusKode;
    }

    public List<DaerahItem> getData() {
        return data;
    }

    public void setData(List<DaerahItem> data) {
        this.data = data;
    }

    public class DaerahItem {

        @SerializedName("id")
        private String id;

        @SerializedName("name")
        private String name;

        @SerializedName("latitude")
        private double latitude;

        @SerializedName("longitude")
        private double longitude;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }
}