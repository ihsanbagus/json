package id.myjson.customer.model;

import com.google.gson.annotations.SerializedName;

public class ProfileModel {

    @SerializedName("nama")
    private String nama;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private String longitude;

    @SerializedName("alamat")
    private String alamat;

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getAlamat() {
        return alamat;
    }

    @Override
    public String toString() {
        return
                "ProfileModel{" +
                        "nama = '" + nama + '\'' +
                        ",user_id = '" + userId + '\'' +
                        ",latitude = '" + latitude + '\'' +
                        ",longitude = '" + longitude + '\'' +
                        ",alamat = '" + alamat + '\'' +
                        "}";
    }
}