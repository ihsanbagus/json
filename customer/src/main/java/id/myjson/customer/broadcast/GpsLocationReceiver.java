package id.myjson.customer.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GpsLocationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.sendBroadcast(new Intent("GPS CHANGED"));
        switch (intent.getAction()) {
            case Intent.ACTION_DATE_CHANGED:
                //what you want to do
                break;
            case Intent.ACTION_BOOT_COMPLETED:
                //what you want to do
                break;
        }
    }
}
