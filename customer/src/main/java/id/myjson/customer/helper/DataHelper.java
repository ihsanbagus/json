package id.myjson.customer.helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class DataHelper {
    public static String generateParam(Map<String, String> params) throws JSONException {
        JSONObject param = new JSONObject();
        for (Map.Entry<String, String> x : params.entrySet()) {
            param.put(x.getKey(), x.getValue());
        }
        return param.toString();
    }
}
