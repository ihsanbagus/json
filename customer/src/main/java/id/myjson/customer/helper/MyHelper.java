package id.myjson.customer.helper;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class MyHelper {

    public static void toggleKeyboard(Activity act) {
        View v = act.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void rippleClick(Context ctx, View v) {
        TypedValue outValue = new TypedValue();
        ctx.getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        v.setBackgroundResource(outValue.resourceId);
        v.setClickable(true);
        v.setFocusable(true);
    }

}
