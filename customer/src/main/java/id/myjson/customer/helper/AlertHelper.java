package id.myjson.customer.helper;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import id.myjson.customer.R;
import com.google.android.material.snackbar.Snackbar;

public class AlertHelper {

    public static void snackbar(View v, String pesan) {
        Snackbar sb = Snackbar.make(v, pesan, Snackbar.LENGTH_SHORT);
        sb.show();
    }

    public static void toast(Context ctx, String pesan) {
        //inflate view
        Activity act = (Activity) ctx;
        View custom_view = act.getLayoutInflater().inflate(R.layout.custom_toast, null);
        ((TextView) custom_view.findViewById(R.id.text)).setText(pesan);

        Toast toast = new Toast(ctx);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(custom_view);
        toast.setGravity(Gravity.CENTER, 0, -500);
        toast.show();
    }
}
