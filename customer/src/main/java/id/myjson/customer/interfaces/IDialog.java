package id.myjson.customer.interfaces;

import android.app.Dialog;

public interface IDialog {
    void onDialogConfirmClickYes(Dialog dialog);

    void onDialogConfirmClickNo(Dialog dialog);
}
