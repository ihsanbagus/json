package id.myjson.customer.interfaces;

import id.myjson.customer.model.ProfileModel;

import java.util.List;

public interface IProfile {
    void onLoadDataSuccess(List<ProfileModel> data);
    void onLoadDataFailure(String pesan);
}
