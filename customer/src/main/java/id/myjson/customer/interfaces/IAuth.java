package id.myjson.customer.interfaces;

import id.myjson.customer.model.AuthModel;

import java.util.List;

public interface IAuth {

    interface ISignin {
        void onLoadDataSuccess(List<AuthModel> data);
        void onLoadDataFailure(String pesan);
    }

    interface ISignup {
        void onLoadDataSuccess(String pesan);
        void onLoadDataFailure(String pesan);
    }

}
