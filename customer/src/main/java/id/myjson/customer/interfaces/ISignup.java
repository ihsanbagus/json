package id.myjson.customer.interfaces;

public interface ISignup {
    void onLoadDataSuccess(String pesan);
    void onLoadDataFailure(String pesan);
}
