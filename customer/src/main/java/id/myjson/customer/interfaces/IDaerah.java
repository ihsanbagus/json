package id.myjson.customer.interfaces;

import java.util.List;

import id.myjson.customer.model.DaerahModel;

public interface IDaerah {
    void onLoadDataSuccess(List<DaerahModel.DaerahItem> data, String from);

    void onLoadDataFailure(String pesan);
}
