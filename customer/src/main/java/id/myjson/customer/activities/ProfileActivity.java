package id.myjson.customer.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import id.myjson.customer.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.myjson.customer.helper.DialogHelper;
import id.myjson.customer.interfaces.IDialog;

public class ProfileActivity extends BaseActivity implements IDialog {

    private DialogHelper dh = new DialogHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bSignout, R.id.bEdit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bSignout:
                dh.Comfirm(this, "Sign Out", "Ingin keluar dari aplikasi ?");
                break;
            case R.id.bEdit:
                break;
        }
    }

    @Override
    public void onDialogConfirmClickYes(Dialog dialog) {

    }

    @Override
    public void onDialogConfirmClickNo(Dialog dialog) {

    }
}
