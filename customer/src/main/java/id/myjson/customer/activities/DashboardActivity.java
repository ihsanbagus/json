package id.myjson.customer.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.PopupMenu;

import id.myjson.customer.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.myjson.customer.helper.DialogHelper;
import id.myjson.customer.interfaces.IDialog;

public class DashboardActivity extends BaseActivity implements IDialog {

    private DialogHelper dh = new DialogHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bSignout, R.id.optionMenu, R.id.bJemput, R.id.bRiwayat, R.id.bBayar})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSignout:
                dh.Comfirm(this, "Sign Out", "Ingin Signout dari aplikasi ?");
                break;
            case R.id.optionMenu:
                options(v);
                break;
            case R.id.bJemput:
                startActivity(new Intent(this, JemputActivity.class));
                break;
            case R.id.bRiwayat:
                break;
            case R.id.bBayar:
                break;
        }
    }

    private void options(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_profile:
                    startActivity(new Intent(this, ProfileActivity.class));
                    break;
                case R.id.menu_setting:
//                    startActivity(new Intent(this, SettingActivity.class));
                    break;
                case R.id.menu_about:
//                    startActivity(new Intent(this, AboutActivity.class));
                    break;
            }
            return true;
        });
        popupMenu.inflate(R.menu.options);
        popupMenu.show();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onDialogConfirmClickYes(Dialog dialog) {
        Intent intent = new Intent(getApplicationContext(), SigninActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onDialogConfirmClickNo(Dialog dialog) {

    }
}