package id.myjson.customer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import id.myjson.customer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.logo)
    AppCompatImageView logo;
    @BindView(R.id.loading)
    AppCompatImageView loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        Glide.with(this).asGif().load(R.drawable.logo).into(logo);
        Glide.with(this).asGif().load(R.drawable.loading).into(loading);

        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, SigninActivity.class));
            finish();
        }, 4500);
    }

    @Override
    public void onBackPressed() {
    }
}
