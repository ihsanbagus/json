package id.myjson.customer.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import id.myjson.customer.R;

public class BaseActivity extends AppCompatActivity {

    public void setToolbar(Toolbar t, String title, boolean canBack) {
        t.setTitle(title);
        if (canBack) {
            t.setNavigationIcon(R.drawable.ic_arrow_left);
            t.setNavigationOnClickListener(v -> finish());
        }
    }
}
