package id.myjson.customer.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.bumptech.glide.Glide;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.myjson.customer.R;
import id.myjson.customer.data.Daerah;
import id.myjson.customer.helper.DialogHelper;
import id.myjson.customer.interfaces.IDaerah;
import id.myjson.customer.interfaces.IDialog;
import id.myjson.customer.model.DaerahModel;

import static id.myjson.customer.helper.AlertHelper.toast;
import static id.myjson.customer.helper.MyHelper.rippleClick;

public class SignupActivity extends BaseActivity implements IDaerah, IDialog {

    @BindView(R.id.tgLahir)
    AppCompatTextView tgLahir;
    @BindView(R.id.jenisKelamin)
    AppCompatTextView jenisKelamin;
    @BindView(R.id.provinsi)
    AppCompatTextView provinsi;
    @BindView(R.id.kota)
    AppCompatTextView kota;
    @BindView(R.id.kecamatan)
    AppCompatTextView kecamatan;
    @BindView(R.id.kelurahan)
    AppCompatTextView kelurahan;
    @BindView(R.id.bSignup)
    AppCompatButton bSignup;
    @BindView(R.id.nama)
    AppCompatEditText nama;
    @BindView(R.id.tempatLahir)
    AppCompatEditText tempatLahir;
    @BindView(R.id.alamat)
    AppCompatEditText alamat;
    @BindView(R.id.rt)
    AppCompatEditText rt;
    @BindView(R.id.rw)
    AppCompatEditText rw;
    @BindView(R.id.noRumah)
    AppCompatEditText noRumah;
    @BindView(R.id.wni)
    AppCompatRadioButton wni;
    @BindView(R.id.wna)
    AppCompatRadioButton wna;
    @BindView(R.id.kewarganegaraan)
    RadioGroup kewarganegaraan;
    @BindView(R.id.noKk)
    AppCompatEditText noKk;
    @BindView(R.id.noKtp)
    AppCompatEditText noKtp;
    @BindView(R.id.ktp)
    AppCompatImageView ktp;
    @BindView(R.id.profile)
    AppCompatImageView profile;
    private Calendar calendar = Calendar.getInstance();
    private int year, month, day;
    private ProgressDialog loading;
    private Daerah prov = new Daerah(this);
    private String provinsiId = "0", kotaId = "0", kecamatanId = "0";
    private AppCompatTextView tv;
    private String kwn = "";
    private String fKtp = "";
    private String fProfile = "";
    private static final int TAKE_KTP = 103;
    private static final int TAKE_PROFILE = 104;
    private DialogHelper dh = new DialogHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
//        showDate(year, month + 1, day);
    }

    @OnClick({R.id.tgLahir, R.id.jenisKelamin, R.id.provinsi, R.id.kota, R.id.kecamatan, R.id.kelurahan, R.id.wni, R.id.wna, R.id.ktp, R.id.profile, R.id.bSignup})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.tgLahir:
                tgLahir.setError(null);
                setDate(v);
                break;
            case R.id.jenisKelamin:
                jenisKelamin.setError(null);
                dialogJenisKelamin();
                break;
            case R.id.provinsi:
                tv = (AppCompatTextView) v;
                prov.getDaerah("", "provinsi");
                loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
                break;
            case R.id.kota:
                tv = (AppCompatTextView) v;
                if (Integer.parseInt(provinsiId) > 0) {
                    prov.getDaerah(provinsiId, "kota");
                    loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
                } else {
                    toast(this, "Pilih Provinsi Dahulu");
                }
                break;
            case R.id.kecamatan:
                tv = (AppCompatTextView) v;
                if (Integer.parseInt(kotaId) > 0) {
                    prov.getDaerah(kotaId, "kecamatan");
                    loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
                } else {
                    toast(this, "Pilih Kota / Kabupaten Dahulu");
                }
                break;
            case R.id.kelurahan:
                tv = (AppCompatTextView) v;
                if (Integer.parseInt(kecamatanId) > 0) {
                    prov.getDaerah(kecamatanId, "kelurahan");
                    loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
                } else {
                    toast(this, "Pilih Kecamatan Dahulu");
                }
                break;
            case R.id.wni:
                wni.setError(null);
                wna.setError(null);
                kwn = "wni";
                break;
            case R.id.wna:
                wni.setError(null);
                wna.setError(null);
                kwn = "wna";
                break;
            case R.id.ktp:
//                Intent takeKtp = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takeKtp, TAKE_KTP);

                //from gallery
                Intent takeKtp = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(takeKtp, TAKE_KTP);
                break;
            case R.id.profile:
                //from camera
//                Intent takeProfile = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takeProfile, TAKE_PROFILE);

                //from gallery
                Intent takeProfile = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(takeProfile, TAKE_PROFILE);
                break;
            case R.id.bSignup:
                if (cekField()) {
                    dh.Comfirm(this, "Konfirmasi Data", "Data Yang Diisi Sudah Benar ?");
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case TAKE_KTP:
                if (resultCode == RESULT_OK) {
                    fKtp = "ok";
                    Uri selectedImage = imageReturnedIntent.getData();
                    Glide.with(this).load(selectedImage).into(ktp);
                }
                break;
            case TAKE_PROFILE:
                if (resultCode == RESULT_OK) {
                    fProfile = "ok";
                    Uri selectedImage = imageReturnedIntent.getData();
                    Glide.with(this).load(selectedImage).into(profile);
                }
                break;
        }
    }

    private boolean cekField() {
        boolean ok = true;
        String pesan = "Wajib Diisi";
        if (TextUtils.isEmpty(nama.getText())) {
            nama.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(tempatLahir.getText())) {
            tempatLahir.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(tgLahir.getText())) {
            tgLahir.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(jenisKelamin.getText())) {
            jenisKelamin.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(alamat.getText())) {
            alamat.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(rt.getText())) {
            rt.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(rw.getText())) {
            rw.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(noRumah.getText())) {
            noRumah.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(provinsi.getText())) {
            provinsi.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(kota.getText())) {
            kota.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(kecamatan.getText())) {
            kecamatan.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(kelurahan.getText()) && !provinsiId.equalsIgnoreCase("91") && !provinsiId.equalsIgnoreCase("94")) {
            kelurahan.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(kwn)) {
            wni.setError(pesan);
            wna.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(noKk.getText())) {
            noKk.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(noKtp.getText())) {
            noKtp.setError(pesan);
            ok = false;
        }
        if (TextUtils.isEmpty(fKtp)) {
            toast(this, "Upload Foto KTP");
            ok = false;
        }
        if (TextUtils.isEmpty(fProfile)) {
            toast(this, "Upload Foto Profil");
            ok = false;
        }
        return ok;
    }

    private void showDate(int year, int month, int day) {
        tgLahir.setText(new StringBuilder().append(day).append("/").append(month).append("/").append(year));
    }

    public void setDate(View view) {
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = (arg0, arg1, arg2, arg3) -> {
        tgLahir.setText(arg3 + "/" + arg2 + "/" + arg1);
        // arg1 = year // arg2 = month // arg3 = day
    };

    private void dialogJenisKelamin() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_jenis_kelamin);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        RadioGroup jk = dialog.findViewById(R.id.jenisKelamin);

        jk.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton checkedRadioButton = group.findViewById(checkedId);
            // This puts the value (true/false) into the variable
            boolean isChecked = checkedRadioButton.isChecked();
            // If the radiobutton that has changed in check state is now checked...
            if (isChecked) {
                // Changes the textview's text to "Checked: example radiobutton text"
                jenisKelamin.setText(checkedRadioButton.getText());
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onLoadDataSuccess(List<DaerahModel.DaerahItem> data, String from) {
        tv.setError(null);
        if (data.size() > 0) {
            showDaerah("Pilih " + from, data, tv, from);
        } else {
            toast(this, "data kosong");
        }
        loading.dismiss();
    }

    @Override
    public void onLoadDataFailure(String pesan) {
        toast(this, pesan);
        loading.dismiss();
    }

    private void showDaerah(String judul, List<DaerahModel.DaerahItem> data, AppCompatTextView z, String from) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_daerah);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        AppCompatTextView title = dialog.findViewById(R.id.title);
        AppCompatImageButton bClose = dialog.findViewById(R.id.bClose);
        LinearLayoutCompat namaDaerah = dialog.findViewById(R.id.namaDaerah);
        title.setText(judul);
        bClose.setOnClickListener(v -> dialog.dismiss());

        for (DaerahModel.DaerahItem x : data) {
            AppCompatTextView a = new AppCompatTextView(this);
            rippleClick(this, a);
            a.setPadding(10, 10, 10, 10);
            a.setTextSize(getResources().getDimension(R.dimen.text_xmedium));
            a.setHint(x.getId());
            a.setText(x.getName());
            a.setOnClickListener(v -> {
                if (from.equalsIgnoreCase("provinsi")) {
                    provinsiId = a.getHint().toString();
                    kotaId = "0";
                    kecamatanId = "0";
                    kota.setText("");
                    kecamatan.setText("");
                    kelurahan.setText("");
                }
                if (from.equalsIgnoreCase("kota")) {
                    kotaId = a.getHint().toString();
                    kecamatanId = "0";
                    kecamatan.setText("");
                    kelurahan.setText("");
                }
                if (from.equalsIgnoreCase("kecamatan")) {
                    kecamatanId = a.getHint().toString();
                    kelurahan.setText("");
                }
                z.setText(a.getText());
            });
            namaDaerah.addView(a);
            View v = new View(this);
            v.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            v.setBackgroundColor(getResources().getColor(R.color.black));
            namaDaerah.addView(v);
        }

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onDialogConfirmClickYes(Dialog dialog) {
        loading = ProgressDialog.show(this, "", "Loading. Please wait...", true);
        new Handler().postDelayed(() -> loading.dismiss(), 5000);
    }

    @Override
    public void onDialogConfirmClickNo(Dialog dialog) {
    }
}
