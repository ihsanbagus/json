package id.myjson.customer.api;

import id.myjson.customer.model.AuthModel;
import id.myjson.customer.model.ProfileModel;
import id.myjson.customer.model.DaerahModel;
import id.myjson.customer.model.ReturnModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("Auth/signin")
    Call<ReturnModel<List<AuthModel>>> getSignin(@Header("X-Api-Key") String auth, @Query("param") String param);

    @FormUrlEncoded
    @POST("Auth/signup")
    Call<ReturnModel> postSignup(@Header("X-Api-Key") String auth, @Field("param") String param);

    @GET("Users")
    Call<ReturnModel<List<ProfileModel>>> getUserBy(@Header("X-Api-Key") String auth, @Query("no_hp") String user_id);

    @FormUrlEncoded
    @POST("Users")
    Call<ReturnModel> addUser(@Header("X-Api-Key") String auth, @Field("param") String param);

    @GET("Wilayah/{uri}/{daerah_id}")
    Call<DaerahModel> getDaerah(@Header("X-Api-Key") String auth, @Path("uri") String uri, @Path("daerah_id") String daerah_id);

}
