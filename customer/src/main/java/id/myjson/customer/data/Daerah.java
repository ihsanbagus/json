package id.myjson.customer.data;

import androidx.annotation.NonNull;

import java.util.List;

import id.myjson.customer.interfaces.IDaerah;
import id.myjson.customer.model.DaerahModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.myjson.customer.api.ApiClient.Authorization;

public class Daerah extends ApiPresenter {
    private IDaerah iDaerah;

    public Daerah(IDaerah iDaerah) {
        this.iDaerah = iDaerah;
    }

    public void getDaerah(String daerah_id, String from) {
        Call<DaerahModel> call = apiService.getDaerah(Authorization, from, daerah_id);
        call.enqueue(new Callback<DaerahModel>() {
            @Override
            public void onResponse(@NonNull Call<DaerahModel> call, @NonNull Response<DaerahModel> response) {
                if (response.body().status()) {
                    List<DaerahModel.DaerahItem> data = response.body().getData();
                    iDaerah.onLoadDataSuccess(data, from);
                } else {
                    iDaerah.onLoadDataFailure(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DaerahModel> call, @NonNull Throwable t) {
                iDaerah.onLoadDataFailure("Tidak dapat terhubung ke Server, periksa koneksi internet atau hubungi Admin.");
                call.cancel();
            }
        });
    }
}
