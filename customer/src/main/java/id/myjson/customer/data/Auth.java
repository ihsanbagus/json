package id.myjson.customer.data;

import androidx.annotation.NonNull;

import id.myjson.customer.api.ApiClient;
import id.myjson.customer.interfaces.IAuth;
import id.myjson.customer.model.AuthModel;
import id.myjson.customer.model.ReturnModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Auth extends ApiPresenter {
    private IAuth.ISignin iSignin;
    private IAuth.ISignup iSignup;

    public Auth(IAuth.ISignin iSignin, IAuth.ISignup iSignup) {
        this.iSignin = iSignin;
        this.iSignup = iSignup;
    }

    public void getSignin(String param) {
        Call<ReturnModel<List<AuthModel>>> call = apiService.getSignin(ApiClient.Authorization, param);
        call.enqueue(new Callback<ReturnModel<List<AuthModel>>>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel<List<AuthModel>>> call, @NonNull Response<ReturnModel<List<AuthModel>>> response) {
                if (response.body().isStatus()) {
                    List<AuthModel> data = response.body().getData();
                    iSignin.onLoadDataSuccess(data);
                } else {
                    iSignin.onLoadDataFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel<List<AuthModel>>> call, @NonNull Throwable t) {
                iSignin.onLoadDataFailure(t.getMessage());
                call.cancel();
            }
        });
    }

    public void postSignup(String param) {
        Call<ReturnModel> call = apiService.postSignup(ApiClient.Authorization, param);
        call.enqueue(new Callback<ReturnModel>() {
            @Override
            public void onResponse(@NonNull Call<ReturnModel> call, @NonNull Response<ReturnModel> response) {
                if (response.body().isStatus()) {
                    ReturnModel data = response.body();
                    iSignup.onLoadDataSuccess(data.getPesan());
                } else {
                    iSignup.onLoadDataFailure(response.body().getPesan());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReturnModel> call, @NonNull Throwable t) {
                iSignin.onLoadDataFailure(t.getMessage());
                call.cancel();
            }
        });
    }
}
